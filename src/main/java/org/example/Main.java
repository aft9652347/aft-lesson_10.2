package org.example;

import org.apache.commons.lang3.StringUtils;

public class Main {
    public static void main(String[] args) {

        String monkeyBusiness = "Мартышка, вот делишки!";

        System.out.println(StringUtils.reverse(monkeyBusiness));
        System.out.println(StringUtils.swapCase(monkeyBusiness));

    }
}